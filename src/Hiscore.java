import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.*;
import java.util.*;

/**
 * Created on 29.10.2015.
 */
public class Hiscore {

    private static int score, index;
    private static String[][] results;
    private static final int HISCORE_TOP_MAX = 10;

    /**
    * Show hiscore window
    */
    public static void display() {

        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Tipptulemused");
        window.setResizable(false);
        window.setMinWidth(300);
        window.initStyle(StageStyle.UTILITY);

        Label label = new Label();
        label.setText("Parimad tulemused:");

        //show hiscore results
        Text places = new Text();
        places.setFill(Color.ALICEBLUE);
        places.setText(hiscoreToText());

        Button closeBtn = new Button("Sulge aken");
        closeBtn.setOnAction(e -> window.close());

        window.addEventHandler(KeyEvent.KEY_PRESSED, event ->  {
            if (event.getCode() == KeyCode.ESCAPE){
                window.close();
            }
        });

        VBox layout = new VBox(10);
        layout.getChildren().addAll(label, places, closeBtn);
        layout.setAlignment(Pos.CENTER);
        layout.setPadding(new Insets(10,0,10,0));

        Scene hiscore = new Scene(layout);
        hiscore.getStylesheets().add("game.css");

        window.setScene(hiscore);
        window.showAndWait();
    }
    /**
    * Makes hiscore results to text
    * @return String
    */
    public static String hiscoreToText() {

        String text = "";
        getResults();
        if (results != null) {
            for (int i = 0; i < results.length; i++) {
                text += (i + 1) + ". " + results[i][0] + " " + results[i][1] + "\n";
            }

        } else {
            text = "Tulemusi ei leitud";
        }

        return text;

    }
    /**
    * Checks if games score is bigger than any of the hiscore scores
    */
    public static void checkScore(int gameScore) {

        score = gameScore;

        if (score != 0) {
            //Get place to put in hiscore
            index = getHiscoreLowestIndex();
            //If score is in hiscore, add to hiscore
            if (index < HISCORE_TOP_MAX) {
                getName();
            }
        }
    }
    /**
    * Gets hiscore results from hiscore file.
    */
    private static void getResults() {
        //use list to avoid null array values
        List<ArrayList<String>> scores = new ArrayList<>();

        try {
            Scanner in = new Scanner(new FileReader("hiscore.txt"));
            //avoid splitting at whitespaces
            in.useDelimiter("\n");
            String line;
            int i = 0;
            while (in.hasNext()) {
                //Add line values to temp file to keep association
                ArrayList<String> temp = new ArrayList<>();
                line = in.next();
                //add everything before last comma
                temp.add(line.substring(0, line.lastIndexOf(',')));
                //add after last comma
                temp.add(line.substring(line.lastIndexOf(',') + 1));
                scores.add(temp);
                i++;
            }

            String array[][] = new String[i][2];
            i = 0;
            //list to two dimensional array for easier later usage
            for (ArrayList a : scores) {
                array[i][0] = a.get(0).toString();
                array[i][1] = a.get(1).toString();
                i++;
            }
            //put scores into results for other methods to use
            results = array;
        }
        catch (FileNotFoundException e) {
            results = null;
        }
    }
    /**
    * Gets lowest value index from hiscore
    * @return int
    */
    private static int getHiscoreLowestIndex() {

        //Get hiscore results
        getResults();
        if (results != null) {
            int i = results.length;
            for (String a[] : results) {
                if (score > Integer.parseInt(a[1])) {
                    i--;
                }
            }
            return i;
        }
        return 0;
    }
    /**
    * Gets players name to put to hiscore
    */
    private static void getName() {
        Stage window = new Stage();

        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle("Tulemuse sisestamine");
        window.setMinWidth(300);
        window.setResizable(false);

        Label label = new Label();
        label.setText("Sisesta enda nimi");

        TextField input = new TextField();

        Button insert = new Button("Lisa tulemustesse");
        insert.setOnAction(event -> {
            if (validateInput(input.getText())) {
                insertToHiscore(input.getText());
                window.close();
            }
        });
        input.addEventHandler(KeyEvent.KEY_PRESSED, event ->  {
            if (event.getCode() == KeyCode.ENTER) {
                if (validateInput(input.getText())) {
                    insertToHiscore(input.getText());
                    window.close();
                }
            }
            if (event.getCode() == KeyCode.ESCAPE) {
                window.close();
            }
        });

        VBox layout = new VBox(20);
        layout.getChildren().addAll(label,input,insert);
        layout.setPadding(new Insets(5,20,5,20));
        layout.setAlignment(Pos.CENTER);

        Scene getName = new Scene(layout, 300, 250);
        getName.getStylesheets().add("game.css");
        window.setScene(getName);
        window.show();
    }

    /**
     * Validates user input
     * @param name text to validate
     * @return boolean
     */
    private static boolean validateInput(String name) {
        return (name.length() < 20 &&
                name.trim().length() > 0
        );
    }
    /**
    * Puts player name and score to hiscore
    * @param name name of the player
    */
    private static void insertToHiscore(String name) {

        try {
            File file = new File("hiscore.txt");
            FileWriter fileWriter = new FileWriter(file);
                //check if there were results before
                if (results != null) {
                    int i = 0;
                    for (String item[] : results) {
                        //skips lowest result if more than hiscore places
                        if (i < HISCORE_TOP_MAX - 1) {
                            //check if entering index and score index are same
                            if (i == index) {
                                //write new result
                                fileWriter.write(name + "," + score + "\n");
                            }
                            //writes previous result
                            fileWriter.write(item[0] + "," + item[1]);
                            //checks if new result is last
                            if (i == results.length - 1 && index > i) {
                                fileWriter.write("\n" + name + "," + score);
                            }
                            i++;
                            //checks where to print new line
                            if (i < results.length && i != HISCORE_TOP_MAX - 1) {
                                fileWriter.write("\n");
                            }
                        }
                    }
                } else {
                    fileWriter.write(name + "," + score);
                }
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Viga faili kirjutamisel");
        }
    }
}