import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created on 29.10.2015.
 */
public class ConfirmExit {

    private static boolean answer;

    /**
    * Shows confirm window
    * @return boolean
    */
    public static boolean display(String title, String message) {
        Stage window = new Stage();

        //must close this window first to use some other window
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(title);
        window.setResizable(false);
        window.setMinWidth(300);
        window.initStyle(StageStyle.UTILITY);

        Label label = new Label();
        label.setText(message);

        Button yesBtn = new Button("Jah");
        Button noBtn = new Button("Ei");

        yesBtn.setOnAction(event -> {
            answer = true;
            window.close();
        });
        noBtn.setOnAction(event -> {
            answer = false;
            window.close();
        });

        window.addEventHandler(KeyEvent.KEY_PRESSED, event ->  {
            if (event.getCode() == KeyCode.ESCAPE) {
                answer = false;
                window.close();
            }
        });

        FlowPane layout = new FlowPane();
        layout.setHgap(30);
        layout.getChildren().addAll(label, yesBtn, noBtn);
        layout.setAlignment(Pos.CENTER);
        layout.setPadding(new Insets(10,10,10,10));


        Scene confirmExit = new Scene(layout);
        confirmExit.getStylesheets().add("game.css");
        window.setScene(confirmExit);
        window.showAndWait();

        return answer;
    }
}

