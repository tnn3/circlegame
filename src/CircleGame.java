import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Created on 29.10.2015.
 * Game was made by using thenewboston tutorials at https://youtu.be/FLkOX4Eez6o
 */
public class CircleGame extends Application {

    private Stage window;
    private Scene sceneGame, gameOver;
    private int scoreInt;
    private Label score, gameOverLabel;
    private final double circleRadius = 75;
    private Circle circle;

    public static void main(String[] args) {
        launch(args);
    }
    /**
    * Build main window
    * @param primaryStage JavaFX main stage name
    */
    @Override
    public void start(Stage primaryStage) throws Exception {

        window = primaryStage;
        window.setTitle("Ringiklikk");
        window.setResizable(false);

        Scene sceneStart = new Scene(buildStart(), 500, 500);
        sceneStart.getStylesheets().add("game.css");

        window.setScene(sceneStart);
        window.show();

        sceneGame = new Scene(buildGame(), 500, 500);
        sceneGame.getStylesheets().add("game.css");

        gameOver = new Scene(buildGameOver(), 500, 500);
        gameOver.getStylesheets().add("game.css");

        //confirm closing when game is running
        window.setOnCloseRequest(event -> {
            if (window.getScene() == sceneGame) {
                event.consume();
                closeProgram();
            }
        });
    }
    /**
    * Build start scene
    */
    private VBox buildStart() {
        Label gameInfo = new Label("Mängu mõte on võimalikult palju ringile vajutada." +
                "\nAega on 10 sekundit.");
        gameInfo.setWrapText(true);

        Button startBtn = new Button("Alusta mängu");
        startBtn.setOnAction( e -> {
            window.setScene(sceneGame);
            startTimer();
        });

        Button hiscoreBtn = new Button("Näita tipptulemusi");
        hiscoreBtn.setOnAction(e -> Hiscore.display());

        VBox startLayout = new VBox(20);
        startLayout.getChildren().addAll(gameInfo, startBtn, hiscoreBtn);
        startLayout.setPadding(new Insets(0,40,0,40));
        startLayout.setAlignment(Pos.CENTER);

        return startLayout;
    }
    /**
    * Build game scene
    */
    private BorderPane buildGame() {
        HBox bottom = new HBox();
        score = new Label("Skoor: 0");

        //make a circle and set its size and location
        circle = new Circle();
        circle.setRadius(circleRadius);
        circle.setCenterX(250);
        circle.setCenterY(250);
        circle.setOnMouseClicked(event -> {
            circle.setCenterX(Math.random() * 330 + 50);
            circle.setCenterY(Math.random() * 330 + 50);
            scoreInt++;
            score.setText("Skoor: " + scoreInt);
            if (scoreInt < 30) {
                circle.setRadius(circleRadius - scoreInt * 2);
            }
        });

        BorderPane playArea = new BorderPane();
        playArea.getChildren().add(circle);

        bottom.getChildren().add(score);

        BorderPane gameLayout = new BorderPane();
        gameLayout.setCenter(playArea);
        gameLayout.setBottom(bottom);

        return gameLayout;
    }
    /**
    * Starts timer to limit game time
    */
    private void startTimer() {
        Timeline timer = new Timeline(new KeyFrame(Duration.seconds(10), e ->
            endGame()
        ));
        timer.play();
    }
    /**
    * Build game over scene
    */
    private VBox buildGameOver() {
        gameOverLabel = new Label("Mäng sai läbi. Tulemuseks tuli: ");

        //Start a new game
        Button newGameBtn = new Button("Alusta uut mängu");
        newGameBtn.setOnAction( e -> {
            resetGame();
            window.setScene(sceneGame);
            startTimer();
        });

        Button hiscoreBtn = new Button("Näita tipptulemusi");
        hiscoreBtn.setOnAction(e -> Hiscore.display());

        VBox gameOverLayout = new VBox(20);
        gameOverLayout.getChildren().addAll(gameOverLabel, newGameBtn, hiscoreBtn);
        gameOverLayout.setAlignment(Pos.CENTER);

        return gameOverLayout;
    }
    /**
    * Reset values for a new game
    */
    private void resetGame() {
        scoreInt = 0;

        //reset cicle values
        circle.setCenterX(250);
        circle.setCenterY(250);
        circle.setRadius(circleRadius);

        score.setText("Skoor: " + scoreInt);
        gameOverLabel.setText("Mäng sai läbi. Tulemuseks tuli: ");
    }
    /**
    * Ends game
    */
    private void endGame() {
        window.setScene(gameOver);
        gameOverLabel.setText(gameOverLabel.getText() + scoreInt);
        Hiscore.checkScore(scoreInt);
    }
    /**
    * Asks for confirmation before exiting program
    */
    private void closeProgram() {
        Boolean result = ConfirmExit.display("Sulge aken","Oled sa kindel, et soovid mängu sulgeda ennem lõppu?");
        if (result) {
            Platform.exit();
        }
    }
}